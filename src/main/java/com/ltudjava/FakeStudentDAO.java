package main.java.com.ltudjava;

import java.util.ArrayList;
import java.util.List;

public class FakeStudentDAO {
    List<Student> students = new ArrayList<>();

    public FakeStudentDAO() {
        students.add(new Student("Nguyen Van A", "Male", "Software Engineering"));
        students.add(new Student("Truong Van B", "Male", "Computer Science"));
        students.add(new Student("Dao Thi C", "Female", "Computer Networking"));
    }

    public Student getStudent(int id){
        if (id >= 0 && id < students.size()){
            return students.get(id);
        } else {
            return null;
        }

    }

    public Student[] getAllStudents(){
        return (Student[])students.toArray();
    }
}

package main.java.com.ltudjava;

public class Student {
    private String name;
    private String gender;
    private String major;

    public Student(String name, String gender, String major) {
        this.name = name;
        this.gender = gender;
        this.major = major;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }
}

package main.java.com.ltudjava;

public class StudentDAO {
    public StudentDAO() {
    }

    public Student getStudent(int id){
        return new Student("Nguyen Van A", "Male", "Software Engineering");
    }

    public Student[] getAllStudents(){
        return new Student[] {
            new Student("Nguyen Van A", "Male", "Software Engineering"),
            new Student("Truong Van B", "Male", "Computer Science"),
            new Student("Dao Thi C", "Female", "Computer Networking")
        };
    }
}

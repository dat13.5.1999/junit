package test.java.com.ltudjava;

import main.java.com.ltudjava.Calculator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AssertionTest6 {
    private final Calculator calculator = new Calculator();

    @Test()
    public void assertDoesNotThrowTestSuccess(){
        assertDoesNotThrow(() -> {
            int number = 69/3;
            System.out.println("assertThrows Test case success: " + number);
        });
    }

    @Test()
    public void assertDoesNotThrowTestFail(){
        assertDoesNotThrow(() -> {
            int number = 69/0;
            System.out.println("assertThrows Test case fail: " + number);
        });
    }
}

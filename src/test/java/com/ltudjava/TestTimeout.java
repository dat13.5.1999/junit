package test.java.com.ltudjava;

import main.java.com.ltudjava.Calculator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestTimeout {
    private final Calculator calculator = new Calculator();

    @Timeout(value=1000, unit = MILLISECONDS)
    @Test()
    void testTimeout() {
        try {
            Thread.sleep(2000);
        } catch(InterruptedException ex){
            System.out.println(ex.getMessage());
        }
    }
}

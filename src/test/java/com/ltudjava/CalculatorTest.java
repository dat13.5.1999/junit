package test.java.com.ltudjava;
import main.java.com.ltudjava.Calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class CalculatorTest {
    private final Calculator calculator = new Calculator();

    @Test
    void testAdd() {
        assertEquals(2, calculator.add(1, 1));
    }

    @Test
    void testAdd2() {
        assertEquals(2, calculator.add(1, 2));
    }
}

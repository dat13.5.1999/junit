package test.java.com.ltudjava;

import main.java.com.ltudjava.Calculator;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestAnnotation {
    private final Calculator calculator = new Calculator();

    @BeforeAll
    static void beforeAll(){
        System.out.println("Before All: Run before any other method");
    }

    @BeforeEach
    void before(){
        System.out.println("Before: Run before each test case");
    }

    @DisplayName("Test Case 1")
    @Test
    void test1() {
        System.out.println("*** Test case 1 ***");
    }

    @DisplayName("Test Case 2")
    @Test
    void test2() {
        System.out.println("*** Test case 2 ***");
    }

    @Disabled
    void testIgnore(){
        System.out.println("*** Ignored Test Case doesn't print anything");
    }

    @AfterEach
    void after(){
        System.out.println("After: Run after each test case");
    }

    @AfterAll
    static void afterAll(){
        System.out.println("After All: Run after all methods are finished");
    }
}

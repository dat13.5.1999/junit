package test.java.com.ltudjava;

import main.java.com.ltudjava.Student;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DummyObject {

    @Test
    void dummyObjectTest() {
        Student[] dummyStudents = new Student[10];
        for (int i = 0; i < 10; i++){
            dummyStudents[i] = createStudent();
        }

        // Do some test with dummy student
        assertEquals(10, dummyStudents.length);
    }
    public Student createStudent(){
        return new Student (null, null, null);
    }
}

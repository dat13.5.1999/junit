package test.java.com.ltudjava;

import main.java.com.ltudjava.Calculator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AssertionTest8 {
    private final Calculator calculator = new Calculator();

    @Test()
    public void assertAllTest(){
        System.out.print("*** Assert All ***");
        assertAll(
                () -> assertEquals(3, calculator.add(1, 1)),
                () -> assertEquals(4, calculator.add(1, 1))
        );
    }

    @Test()
    public void multipleAssertTest(){
        System.out.print("*** Multiple Assert ***");
        assertEquals(3, calculator.add(1, 1));
        assertEquals(4, calculator.add(1, 1));
    }
}

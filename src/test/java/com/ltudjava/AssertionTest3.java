package test.java.com.ltudjava;

import main.java.com.ltudjava.Calculator;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class AssertionTest3 {
    private final Calculator calculator = new Calculator();

    @Test
    public void assertArrayEqualsTest(){
        int[] arr1 = {1, 2, 3};
        int[] arr2 = {1, 2, 3};
        assertArrayEquals(arr1, arr2);
    }

    @Test
    public void assertLinesMatchTest(){
        List<String> list1 = new ArrayList<>();
        list1.add("A");
        list1.add("B");
        list1.add("C");

        List<String> list2 = new ArrayList<>();
        list2.add("A");
        list2.add("B");
        list2.add("C");

        assertLinesMatch(list1, list2);
    }

    @Test
    public void assertIterableEqualsTest(){
        List<Integer> list1 = new ArrayList<>();
        list1.add(1);
        list1.add(2);
        list1.add(3);

        List<Integer> list2 = new ArrayList<>();
        list2.add(1);
        list2.add(2);
        list2.add(3);

        assertIterableEquals(list1, list2);
    }
}

package test.java.com.ltudjava;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

public class ParameterizedTestt2 {
    @ParameterizedTest
    @NullSource
    public void nullSource(String text){
        System.out.println("*** Null Source Test ***");
        System.out.println("Is null: " + (text == null));
    }

    @ParameterizedTest
    @EmptySource
        public void emptySource(String text){
        System.out.println("*** Empty Source Test ***");
        System.out.println("Is empty: " + (text.trim().isEmpty()));
    }

    @ParameterizedTest
    @NullAndEmptySource
    public void nullAndEmptySource(String text){
        System.out.println("*** Null And Empty Source Test ***");
        System.out.println("Is null: " + (text == null));
        System.out.println("Is empty: " + (text != null && text.trim().isEmpty()));
    }
}


package test.java.com.ltudjava;

import main.java.com.ltudjava.Calculator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AssertionTest5 {
    private final Calculator calculator = new Calculator();

    @Test()
    public void assertThrowsTestSuccess(){
        assertThrows(ArithmeticException.class, () -> {
            int number = 69/0;
            System.out.println("assertThrows Test case success: " + number);
        });
    }

    @Test()
    public void assertThrowsTestFail(){
        assertThrows(ArithmeticException.class, () -> {
            int number = 69/3;
            System.out.println("assertThrows Test case fail: " + number);
        });
    }
}

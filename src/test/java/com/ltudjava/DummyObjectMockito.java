package test.java.com.ltudjava;

import main.java.com.ltudjava.Student;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

public class DummyObjectMockito {

    @Test
    void dummyObjectTest() {
        Student[] dummyStudents = new Student[10];
        for (int i = 0; i < 10; i++){
            dummyStudents[i] = mock(Student.class);
        }

        // Do some test with dummy student
        assertEquals(10, dummyStudents.length);
    }
}

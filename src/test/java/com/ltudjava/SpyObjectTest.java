package test.java.com.ltudjava;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class SpyObjectTest {
    @Test
    public void spyObjectTest(){
        List<Integer> list = new ArrayList<>();
        List<Integer> mockList = spy(list);

        // Call real method
        mockList.add(1);
        mockList.add(2);
        mockList.add(3);

        for (Integer number : mockList){
            System.out.print(number + " ");
        }

        // Call fake method
        when(mockList.size()).thenReturn(100);

        assertEquals(2, mockList.size());
    }
}

package test.java.com.ltudjava;

import main.java.com.ltudjava.Calculator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AssertionTest2 {
    private final Calculator calculator = new Calculator();

    @Test
    public void assertEqualsTest(){
        assertEquals(3, calculator.add(1, 1));
    }

    @Test
    public void assertSameTest(){
        String a = "a";
        String b = "b";
        assertSame(a, b);
    }
}

package test.java.com.ltudjava;

import main.java.com.ltudjava.Calculator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AssertionTest1 {
    private final Calculator calculator = new Calculator();

    @Test
    public void assertEqualsTest(){
        assertEquals(2, calculator.add(1, 1));
    }

    @Test
    public void assertNotEqualsTest(){
        assertNotEquals(1, calculator.add(1, 1));
    }

    @Test
    public void assertTrueTest(){
        assertTrue((calculator.add(1, 1) == 2));
    }

    @Test
    public void assertFalseTest(){
        assertFalse((calculator.add(1, 1) != 2));
    }

    @Test
    public void assertNullTest(){
        String a = null;
        assertNull(a);
    }

    @Test
    public void assertNotNullTest(){
        String a = "a";
        assertNotNull(a);
    }

    @Test
    public void assertSameTest(){
        String a = "a";
        String a2 = "a";
        assertSame(a, a2);
    }

    @Test
    public void assertNotSameTest(){
        String a = "a";
        String b = "b";
        assertNotSame(a, b);
    }
}

package test.java.com.ltudjava;

import main.java.com.ltudjava.Student;
import main.java.com.ltudjava.StudentDAO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class StubObjectMockito {
    @Test
    public void stubObjectTest(){
        // Define studentDAO stub
        StudentDAO studentDAO = mock(StudentDAO.class);
        Mockito.when(studentDAO.getStudent(1)).thenReturn(new Student("Huynh Van D", "Male", "Computer Vision"));
        Mockito.when(studentDAO.getAllStudents()).thenReturn(
                new Student[]{
                        new Student("Huynh Van D", "Male", "Computer Vision"),
                        new Student("Banh Thi E", "Female", "Computer Networking"),
                        new Student("Pham Van F", "Male", "Computer Science")
                });

        // Get student
        Student testStudent = studentDAO.getStudent(1);
        System.out.println("Student: " + testStudent.getName());

        // Get all students
        Student[] allStudents = studentDAO.getAllStudents();
        System.out.print("Students: ");
        for (Student student : allStudents){
            System.out.print(student.getName() + ", ");
        }
    }
}


package test.java.com.ltudjava;

import main.java.com.ltudjava.Email;
import main.java.com.ltudjava.EmailCentral;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class MockObjectTest {
    @Test
    public void mockObjectTest(){
        Email emailMock = mock(Email.class);
        EmailCentral emailCentral = new EmailCentral(emailMock);
        emailCentral.sendHappyBirthDayEmail();
        verify(emailMock).sendEmail("Happy birthday !!!");
    }
}


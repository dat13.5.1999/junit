package test.java.com.ltudjava;

import main.java.com.ltudjava.Calculator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AssertionTest7 {
    private final Calculator calculator = new Calculator();

    @Test()
    public void assertAllTestSuccess(){
        assertAll(
                () -> assertEquals(2, calculator.add(1, 1)),
                () -> assertNotEquals(3, calculator.add(1, 1))
        );
    }

    @Test()
    public void assertAllTestFail(){
        assertAll(
                () -> {throw new ArithmeticException("Arithmetic Exception Message");},
                () -> assertNotEquals(3, calculator.add(1, 1))
        );
    }
}

package test.java.com.ltudjava;

import main.java.com.ltudjava.Calculator;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class AssertionTest4 {
    private final Calculator calculator = new Calculator();

    @Test()
    public void assertTimeoutTestSuccess(){
        assertTimeout(Duration.ofMillis(1000), () -> {
            Thread.sleep(500);
        });
    }

    @Test()
    public void assertTimeoutTestFail(){
        assertTimeout(Duration.ofMillis(1000), () -> {
            Thread.sleep(1500);
        });
    }
}

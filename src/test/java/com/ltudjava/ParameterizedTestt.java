package test.java.com.ltudjava;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class ParameterizedTestt {
    // int parameters
    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5})
    public void parameterizedTestNumbers(int number){
        System.out.print("Parameterized Test 1: " + number);
    }

    // String parameters
    @ParameterizedTest
    @ValueSource(strings = {"String 1", "String 2", "String 3", "String 4", "String 5"})
    public void parameterizedTestStrings(String text){
        System.out.print("Parameterized Test 2: " + text);
    }
}

